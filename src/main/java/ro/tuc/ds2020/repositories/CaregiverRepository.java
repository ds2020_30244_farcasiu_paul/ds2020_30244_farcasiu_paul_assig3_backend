package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Caregiver;

import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

    @Transactional
    void deleteCaregiverByName(String name);

    @Transactional
    @Modifying
    @Query("UPDATE Caregiver c SET c.address = :address WHERE c.name = :caregiverName")
    void updateAddress(String address,String caregiverName);

    @Transactional
    Optional<Caregiver> findCaregiverByName(String name);

}
