package ro.tuc.ds2020.assign3;

import net.minidev.json.JSONObject;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;
import java.util.UUID;

public class PillDispenser implements PillDispenserService {

    private MedicationPlanService medicationPlanService;
    private PatientService patientService;

    public PillDispenser(MedicationPlanService medicationPlanService, PatientService patientService)
    {
        this.medicationPlanService=medicationPlanService;
        this.patientService=patientService;
    }


    @Override
    public List<JSONObject> medicationPlans(UUID id) {
        return medicationPlanService.findMedicationPlanByPatientId(id);
    }
}
