package ro.tuc.ds2020.assign3;

import net.minidev.json.JSONObject;

import java.util.List;
import java.util.UUID;

public interface PillDispenserService {

    List<JSONObject> medicationPlans(UUID id);

}
