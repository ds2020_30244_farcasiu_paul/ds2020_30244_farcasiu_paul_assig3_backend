package ro.tuc.ds2020.assign3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

@Configuration
public class Config {

    @Autowired
    private MedicationPlanService medicationPlanService;


    @Autowired
    private PatientService patientService;


    @Bean(name = "/pillDispenser")
    RemoteExporter HessianService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new PillDispenser(medicationPlanService, patientService));
        exporter.setServiceInterface(PillDispenserService.class);
        return exporter;
    }

}
