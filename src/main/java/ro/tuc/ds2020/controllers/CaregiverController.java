package ro.tuc.ds2020.controllers;


import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody CaregiverDetailsDTO caregiverDTO) {
        UUID caregiverID = caregiverService.insertCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @DeleteMapping(value="/{name}")
    public ResponseEntity<JSONObject> deleteCaregiver(@PathVariable("name") String name)
    {
        caregiverService.deleteByName(name);
        JSONObject obj=new JSONObject();
        obj.put("DELETE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @PutMapping(value="/update/{address}/{name}")
    public ResponseEntity<JSONObject> updateCaregiver(@PathVariable("address") String address, @PathVariable("name") String caregiverName)
    {
        caregiverService.updateCaregiverAddress(address,caregiverName);
        JSONObject obj=new JSONObject();
        obj.put("UPDATE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @GetMapping(value = "/get/{name}")
    public ResponseEntity<CaregiverDetailsDTO> getCaregiver(@PathVariable("name") String name) {
        CaregiverDetailsDTO dto = caregiverService.findCaregiverByName(name);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
