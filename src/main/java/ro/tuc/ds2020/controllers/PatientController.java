package ro.tuc.ds2020.controllers;


import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public PatientController(PatientService patientService, MedicationPlanService medicationPlanService) {
        this.patientService = patientService;
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PatientDetailsDTO patientDTO) {
        UUID patientID = patientService.insertPatient(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @DeleteMapping(value="/{name}")
    public ResponseEntity<JSONObject> deletePatient(@PathVariable("name") String name)
    {
        patientService.deleteByName(name);
        JSONObject obj=new JSONObject();
        obj.put("DELETE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @PutMapping(value="/update/{address}/{name}")
            public ResponseEntity<JSONObject> updatePatient(@PathVariable("address") String address, @PathVariable("name") String patientName)
    {
        patientService.updatePatientAddress(address,patientName);
        JSONObject obj=new JSONObject();
        obj.put("UPDATE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @GetMapping(value="/{caregiverpatient}")
    public ResponseEntity<List<PatientDTO>> getPatientsByName(@Valid @PathVariable("caregiverpatient") String name) {
        List<PatientDTO> dtos = patientService.findPatientsWithCaregiver(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value="/find/{caregiverpatient}")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlansForSpecific(@Valid @PathVariable("caregiverpatient") String name) {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlan();
        List<PatientDTO> dtos2 = patientService.findPatientsWithCaregiver(name);
        System.out.println(dtos.size());
        List<MedicationPlanDTO> dtos3 = new ArrayList<>();
        for (MedicationPlanDTO l:dtos) {
            boolean var=false;
            for (PatientDTO p:dtos2) {
                if (l.getPatientid().toString().equals(p.getId().toString())) {
                    dtos3.add(l);
                    break;
                }
            }
        }
        return new ResponseEntity<>(dtos3, HttpStatus.OK);
    }
    
}
