package ro.tuc.ds2020.controllers;


import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody MedicationDetailsDTO medicationDTO) {
        UUID medicationID = medicationService.insertMedication(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @DeleteMapping(value="/{name}")
    public ResponseEntity<JSONObject> deleteMedication(@PathVariable("name") String name)
    {
        medicationService.deleteByName(name);
        JSONObject obj=new JSONObject();
        obj.put("DELETE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }

    @PutMapping(value="/update/{effects}/{name}")
    public ResponseEntity<JSONObject> updateMedication(@PathVariable("effects") String effects, @PathVariable("name") String medicationName)
    {
        medicationService.updateMedicationSideEffects(effects,medicationName);
        JSONObject obj=new JSONObject();
        obj.put("UPDATE", "success");
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
}
