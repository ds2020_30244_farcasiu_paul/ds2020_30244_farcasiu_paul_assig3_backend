package ro.tuc.ds2020.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.UserInfoService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;
    private final UserInfoService userInfoService;
    private final PatientService patientService;

    public MedicationPlanController(MedicationPlanService medicationPlanService, PatientService patientService, UserInfoService userInfoService, PatientService patientService1) {
        this.medicationPlanService = medicationPlanService;
        this.userInfoService = userInfoService;

        this.patientService = patientService1;
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody MedicationPlanDetailsDTO medicationPlanDTO) {
        System.out.println(medicationPlanDTO.toString());
        UUID medicationPlanID = medicationPlanService.insertMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanID, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlan();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value="/{patientid}")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans(@Valid @PathVariable("patientid") UUID patientid) {
        UserInfoDetailsDTO userInfoDTO= userInfoService.findUserById(patientid);
        PatientDetailsDTO patient= patientService.findPatientByName(userInfoDTO.getName());
        List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlanById(patient.getId());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    

}
