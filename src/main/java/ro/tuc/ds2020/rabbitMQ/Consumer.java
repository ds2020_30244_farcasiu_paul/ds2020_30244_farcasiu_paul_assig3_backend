package ro.tuc.ds2020.rabbitMQ;

import net.minidev.json.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivitiesDetailsDTO;
import ro.tuc.ds2020.services.ActivitiesService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component
public class Consumer {

    private ActivitiesService activitiesService;

    @Autowired
    public Consumer(ActivitiesService activitiesService) {
        this.activitiesService = activitiesService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}", containerFactory = "jsaFactory")
    public void receiveMessage(JSONObject activity) {
        //System.out.println("Received " + activity);
        UUID patient_id=UUID.fromString(activity.getAsString("patient_id"));
        String activityName=activity.getAsString("activity_name");
        String rule="";
        String start_date=activity.getAsString("start_date");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDate= LocalDateTime.parse(start_date,formatter);
        String end_date=activity.getAsString("end_date");
        LocalDateTime endDate= LocalDateTime.parse(end_date,formatter);

        rule=newRule(activityName ,startDate, endDate);
        
        ActivitiesDetailsDTO activitiesDetailsDTO=new ActivitiesDetailsDTO(patient_id, activityName, startDate, endDate, rule);
        activitiesService.insertActivities(activitiesDetailsDTO);
        System.out.println(activitiesDetailsDTO.toString());
    }

    private String newRule(String activityName, LocalDateTime startDate, LocalDateTime endDate)
    {
        String rule ="";
        long duration = Duration.between(startDate, endDate).toMillis()/1000;

        if(activityName.equals("Sleeping") && duration > 7*60*60){
            rule="Sleep period longer than 7hours";
        }
        if(activityName.equals("Leaving") && duration > 5*60*60){
            rule="The leaving activity (outdoor) is longer than 5hours";
        }
        if(activityName.equals("Toileting") && duration > 30*60){
            rule="Period spent in bathroom is longer than 30 minutes";
        }
        if(activityName.equals("Showering") && duration > 30*60){
            rule="Period spent in bathroom is longer than 30 minutes";
        }
        if(activityName.equals("Grooming") && duration > 30*60){
            rule="Period spent in bathroom is longer than 30 minutes";
        }
        return rule;
    }

}