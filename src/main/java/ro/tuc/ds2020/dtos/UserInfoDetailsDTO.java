package ro.tuc.ds2020.dtos;


import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class UserInfoDetailsDTO {

    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String userType;
    @NotNull
    private String name;

    public UserInfoDetailsDTO() {
    }

    public UserInfoDetailsDTO( String username, String password, String userType, String name)
    {
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.name = name;
    }

    public UserInfoDetailsDTO(UUID id, String username, String password, String userType,String name)
    {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfoDetailsDTO userInfoDetailsDTO = (UserInfoDetailsDTO) o;
        return Objects.equals(username, userInfoDetailsDTO.username) && Objects.equals(password, userInfoDetailsDTO.password) &&
                Objects.equals(userType, userInfoDetailsDTO.userType) && Objects.equals(name, userInfoDetailsDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, userType,name);
    }
}
