package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {
    private UUID id;
    private UUID patientid;
    private String medication;
    private String interval;
    private LocalDateTime dateStart;
    private LocalDateTime dateEnd;



    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(UUID id, UUID patient_id,String medication, String interval, LocalDateTime dateStart, LocalDateTime dateEnd)
    {
        this.id = id;
        this.patientid=patient_id;
        this.medication = medication;
        this.interval = interval;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public UUID getPatientid() {
        return patientid;
    }

    public void setPatientid(UUID patientid) {
        this.patientid = patientid;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDateTime dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patientid, that.patientid) &&
                Objects.equals(medication, that.medication) &&
                Objects.equals(interval, that.interval) &&
                Objects.equals(dateStart, that.dateStart) &&
                Objects.equals(dateEnd, that.dateEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, patientid, medication, interval, dateStart, dateEnd);
    }
}
