package ro.tuc.ds2020.dtos;


import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicationDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String effects;
    @NotNull
    private int dosage;

    public MedicationDetailsDTO() {
    }

    public MedicationDetailsDTO(String name, String effects, int dosage)
    {
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(UUID id, String name, String effects, int dosage)
    {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDetailsDTO medicationDetailsDTO = (MedicationDetailsDTO) o;
        return Objects.equals(name, medicationDetailsDTO.name) && Objects.equals(effects, medicationDetailsDTO.effects) &&
                dosage == medicationDetailsDTO.dosage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, effects, dosage);
    }
}
