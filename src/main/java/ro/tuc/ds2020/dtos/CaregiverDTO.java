package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<MedicationDTO> {
    private UUID id;
    private String name;
    private Date birth;
    private String gender;
    private String address;


    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID id, String name, Date birth, String gender, String address)
    {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(name, caregiverDTO.name) && Objects.equals(birth, caregiverDTO.birth) &&
                Objects.equals(gender, caregiverDTO.gender) && Objects.equals(address, caregiverDTO.address)
                ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birth, gender, address);
    }
}
