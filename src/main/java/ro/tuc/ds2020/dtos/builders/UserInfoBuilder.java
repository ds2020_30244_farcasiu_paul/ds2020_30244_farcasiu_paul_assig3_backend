package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.UserInfoDTO;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.entities.UserInfo;



public class UserInfoBuilder {

    private UserInfoBuilder() {
    }

    public static UserInfoDTO toUserInfoDTO(UserInfo userInfo) {
        return new UserInfoDTO(userInfo.getId(), userInfo.getUsername(), userInfo.getPassword(), userInfo.getUserType(),userInfo.getName());
    }

    public static UserInfoDetailsDTO toUserInfoDetailsDTO(UserInfo userInfo) {
        return new UserInfoDetailsDTO(userInfo.getId(), userInfo.getUsername(), userInfo.getPassword(), userInfo.getUserType(), userInfo.getName());
    }

    public static UserInfo toEntity(UserInfoDetailsDTO userInfoDetailsDTO) {
        return new UserInfo(userInfoDetailsDTO.getUsername(),
                userInfoDetailsDTO.getPassword(),
                userInfoDetailsDTO.getUserType(),
                userInfoDetailsDTO.getName());
    }
}
