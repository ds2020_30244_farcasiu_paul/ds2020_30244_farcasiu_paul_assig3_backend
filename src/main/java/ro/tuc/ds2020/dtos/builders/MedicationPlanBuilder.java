package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.entities.MedicationPlan;




public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medication) {
        return new MedicationPlanDTO(medication.getId(), medication.getPatientid(),medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd());
    }

    public static MedicationPlanDetailsDTO toMedicationPlanDetailsDTO(MedicationPlan medication) {
        return new MedicationPlanDetailsDTO(medication.getPatientid(),medication.getId(),medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd());
    }

    public static MedicationPlan toEntity(MedicationPlanDetailsDTO medication) {
        return new MedicationPlan( medication.getPatientid(),medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd());
    }
}
