package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.ActivitiesDTO;
import ro.tuc.ds2020.dtos.ActivitiesDetailsDTO;
import ro.tuc.ds2020.entities.Activities;

import java.time.LocalDateTime;
import java.util.UUID;

public class ActivitiesBuilder {

    private ActivitiesBuilder() {
    }

    public static ActivitiesDTO toActivitiesDTO(Activities activities) {
        return new ActivitiesDTO(activities.getId(), activities.getPatient_id(), activities.getActivity(), activities.getStart_date(), activities.getEnd_date(), activities.getRule());
    }

    public static ActivitiesDetailsDTO toActivitiesDetailsDTO(Activities activities) {
        return new ActivitiesDetailsDTO(activities.getId(), activities.getPatient_id(), activities.getActivity(),activities.getStart_date(), activities.getEnd_date(), activities.getRule());
    }

    public static Activities toEntity(ActivitiesDetailsDTO activitiesDetailsDTO) {
        return new Activities(activitiesDetailsDTO.getPatient_id(),
                activitiesDetailsDTO.getActivity(),
                activitiesDetailsDTO.getStart_date(),
                activitiesDetailsDTO.getEnd_date(),
                activitiesDetailsDTO.getRule());
    }
}
